$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("#alertContainer").hide();
    $("#secondLogin").hide();
});


function mostrarAlerta(texto, tipo) {
    $('#alertContainer label').text(texto);
    $("#alertContainer").addClass(tipo);
    $("#alertContainer").show();

}

function closeAlert(){
    $("#alertContainer").hide();
}