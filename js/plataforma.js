$(document).ready(
    function () {
        // auth usuario
        $("#primerLogin").submit(function (event) {
            event.preventDefault();
            auth();
        });
        // auth2 usuario
        $("#segundoLogin").submit(function (event) {
            event.preventDefault();
            secondAuth();
        });
        // crear usuario
        $("#crearUsuario").submit(function (event) {
            event.preventDefault();
            registrar();
        });

    })

function registrar() {
    // obtener datos de modal.
    var formData = {
        username: $("#username").val(),
        nombres: $("#nombres").val(),
        apellidoPaterno: $("#apellidoPaterno").val(),
        apellidoMaterno: $("#apellidoMaterno").val(),
        rut: $("#rut").val(),
        dv: $("#dv").val(),
        password: $("#password").val(),
        email: $("#email").val(),
        telefono: $("#telefono").val(),
        roles: [
            { id: 1 }
        ],
    };
    // metodo POST.
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://3.15.185.34:8080/api/v1/usuarios",
        data: JSON.stringify(formData),
        dataType: 'json',
        success: function (result) {
            // if (result.status === "success") {
            mostrarAlerta("Usuario creado exitosamente", "alert alert-success alert-dismissible");
            // } else {
            //     mostrarAlerta("Error al registrar usuario", "alert alert-danger alert-dismissible");
            // }
            $('#crearUsuario').trigger("reset");
            // Your delay in milliseconds
            var delay = 5000;
            setTimeout(function () { window.location = "./index.html"; }, delay);
        },
        error: function (e) {
            mostrarAlerta("Error al registrar Usuario", "alert alert-danger alert-dismissible");
            console.log("ERROR: ", e);
        }
    });
}

function auth() {
    // obtener datos de modal.

    var user = $("#username").val();
    var pwd = $("#inputPassword").val();

    // metodo PUT.
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://3.15.185.34:8080/api/v1/usuarios/auth?pwd=" + pwd + "&user=" + user,
        // data: JSON.stringify(formData),
        dataType: 'json',
        success: function (result) {
            if (result.mensaje === null || result.mensaje === undefined) {
                mostrarAlerta("Nombre de usuario o contraseña incorrecta", "alert alert-danger alert-dismissible");
                // mostrar cuadro de codigo
            } else {
                $("#firstLogin").hide();
                $("#secondLogin").show();
                localStorage.username = user;
                // { window.location = "./dash.html"; }
            }
        },
        error: function (e) {
            mostrarAlerta("Nombre de usuario o contraseña incorrecta", "alert alert-danger alert-dismissible");
            console.log("ERROR: ", e);
        }
    });
}

function secondAuth() {
    // obtener datos de modal.

    var user = localStorage.username;
    var code = $("#code").val();

    // metodo PUT.
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://3.15.185.34:8080/oauth/token?" +
            "grant_type=password&username=" + user + "&password=" + code,
        headers: {
            'Authorization': 'Basic cGxhdGFmb3JtYWZyb250Om1pbnB1YmxpY28=',
            'Content-Type': 'application/json'//,
        },
        // beforeSend: function (xhr) {
        //     xhr.setRequestHeader("Authorization", "Basic " + btoa("plataformafront:minpublico"));
        // },
        // data: "{}",
        crossDomain: true,
        dataType: 'json',
        success: function (result) {
            console.log(result)
            if (result.access_token === null || result.access_token === undefined) {
                mostrarAlerta("Código de validación incorrecto", "alert alert-danger alert-dismissible");
            } else {
                localStorage.username = user;
                localStorage.access_token = result.access_token;
                { window.location = "./dash.html"; }
            }
        },
        error: function (e) {
            mostrarAlerta("Código de validación incorrecto", "alert alert-danger alert-dismissible");
        }
    });
}